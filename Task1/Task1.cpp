#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

int nextSmallerThan(int);
void nextSmallerThan(string, string);
void nextSmallerThanTests();
bool nextSmallerThanTests(string, string);

int main()
{
    nextSmallerThanTests();
    nextSmallerThan("input.txt", "result.txt");
    nextSmallerThanTests("input.txt", "result.txt");
    system("pause");
}


int powpow(int a, int b) {
    int c = a;
    if(b == 0) return 1;
    for(int i{ 1 }; i < b; i++) {
        c*=a;
    }
    return c;
}

int nextSmallerThan(int number)
{
    //TODO
    // {
    int length{ 0 };
    int i = number;

    while (i != 0) {
        i /= 10;
        length++;
    }

    int* array = new int[length];

    i = number;

    for (int j{ length - 1 }; j >= 0; j--) {
        array[j] = i % 10;
        i /= 10;
    }// дебют энд

    for (int i = number - 1; i > 9; i--) {
        int length2{ 0 };
        //////////////////
        int a = i;
        while (a != 0) {
            a /= 10;
            length2++;
        }

        if (length != length2) {
            return -1;
        }
        //////////////////
        //a = i;
        //if(a % powpow(10, length - 1) < 10) {
        //    return -1;
        //}
        //////////////////
        int* array2 = new int[length];
        int* array3 = new int[length];

        int copyNumber2 = i;

        for (int h{ length - 1 }; h >= 0; h--) {
            array2[h] = copyNumber2 % 10;
            copyNumber2 /= 10;
        }//Миттельшпиль энд

        int colvo = 0;
        for (int k{ 0 }; k < length; k++) {
            int good = -1;

            for (int l{ 0 }; l < length; l++) {
                if (array[k] == array2[l]) {
                    array3[l] = array2[l];
                    array2[l] = -1;
                    good = 0;
                    colvo += 1;
                }
                if (colvo == length) {
                    int number = 0;
                    for (int i = 0; i < length; i++) {
                        number += array3[i] * powpow(10, length - i - 1);
                    }
                    return number;
                }
                if (good == 0) {
                    break;
                }
            }
            if (good == -1) {
                break;
            }
        }
    }
    // }
    return -1;
}

void nextSmallerThan(string input, string output)
{
    //TODO
    // {
        
    string str;
    
    ifstream file(input);
    ofstream result(output);
    
    while (getline(file, str))
    {
        result << nextSmallerThan(stoi(str));
    }
    file.close();
    result.close();

    // }
}

bool nextSmallerThanTests(string input, string output)
{
    ifstream in1(input);
    ifstream in2(output);
    string source1 = "";
    string source2 = "";
    while (getline(in1, source1) && getline(in2, source2))
    {
        int n1 = stoi(source1);
        int n2 = stoi(source2);
        if (n1 != n2)
        {
            in1.close();
            in2.close();
            return false;
        }
    }
    in1.close();
    in2.close();
    return true;
}

void nextSmallerThanTests()
{
    cout << "Test " << (nextSmallerThan(21) == 12 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(531) == 513 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(2071) == 2017 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(9) == -1 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(111) == -1 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(135) == -1 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(1027) == -1 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(1113211111) == 1113121111 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(91234567) == 79654321 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(173582) == 173528 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(4321234) == 4314322 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThan(2147483647) == 2147483476 ? "Passed." : "Failed.") << endl;
    cout << "Test " << (nextSmallerThanTests("result.txt", "output.txt") ? "Passed." : "Failed.") << endl;
}