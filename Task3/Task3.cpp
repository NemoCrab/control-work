#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);

int main()
{
  calculateSumTest("test.txt");
}

int* copyArray(int* array, int size) {
  int* new_array = new int[size];

  for (int i = 0; i < size; i++) {
    new_array[i] = array[i];
  }

  return new_array;
}

int calculateSum(string source)
{
  int sum{ 0 };
  string number = "";  

  for (int i{ 0 }; i < source.size(); i++) {
    if (source[i] == '1' || source[i] == '0') {
      number += source[i];
      
      if ( i == source.size() - 1 ) {
          sum += stoi(number, nullptr, 2);
          break;
      }  
    }

    else if (number != "") {
      sum += stoi(number, nullptr, 2);
      number = "";
    }
  }
  
  
  return sum;
}

int calculateSumFromFile(string fileName)
{
  //TODO
    // {
    ifstream file(fileName);
    int GREAT_SUM_OF_GREAT_TASK{ 0 };
    string str;
    
    while(getline(file, str))
    {
        GREAT_SUM_OF_GREAT_TASK += calculateSum(str);
    }
    return GREAT_SUM_OF_GREAT_TASK;
    // }
}

void calculateSumTest(string fileName)
{
  bool result = calculateSum("1+-0100+*** 1000") == 13;
  cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
  result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
  cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
  result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
  cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
  result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
  cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
  result = calculateSumFromFile(fileName) == 4262515;
  cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}
